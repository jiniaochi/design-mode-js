# 《深入浅出 vue.js》 随书代码和笔记

## 第一章

vue.js简介，不说了（重点JavaScript的元编程，即借助基础的语言特征实现相相对新的语言特征，如生成器和`anyc/await`的关系）

## 第二章

vue.js 对Object的变化追踪， 重点阐述`Dep`类、`Watcher`类、`Observer`类与核心函数`defineReactive`的关系和演变过程。
回答两个问题：

1. 如何追踪变化；
2. 如何收集依赖，即变化后该触发哪些回调函数。

 