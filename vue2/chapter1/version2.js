/**
 * version2： 用数组存储属性值变化后 回调函数 队列，并调用之
 * 收集依赖: getter收集依赖，setter触发回调
 */
const window = {}; // nodejs 环境没有window

function defineReactive(obj={}, key='',val){
    const deps = [];
    Object.defineProperty(
        obj,
        key,
        {
            enumerable: true,
            configurable: true,
            get: function(){
                console.log(window.target.toString());
                deps.push(window.target); 
                // 问题就在于依赖（回调函数）放在哪儿，或者说如何取得（因为get不能设置参数，且需要运行时的数据，所以不能设置为参数,
                // 这里假设存在window.target上，vue2.6实际存在Dep.target上）
                
                window.target = undefined;
                return val;             
            },
            set: function(newVal){
                if(val==newVal){
                    return;
                }
                console.log(deps);
                for(let item of deps){
                    item.call(this);
                }
                val = newVal;
            }
        }
    )
}

const obj = { name:'dwqjdw',age:18 };

window.target = function(){
    console.log('这是 name 属性改变的回调');
}
defineReactive(obj,'name',obj.name);
obj.name; // 先设置全局收集点，再收集依赖

window.target = function(){
    console.log('这是 age 属性改变的回调');
}
defineReactive(obj,'age',obj.age);
obj.age; // 先设置全局收集点，再收集依赖

obj.name = 'fnewjfnewjkfnewkjnc';  // 只触发name的回调，因为name和age调用了两次函数，有不同的闭包
// obj.age = 488;

console.log('==============end============');