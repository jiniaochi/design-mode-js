// version1 监听属性的 值改变，但是缺少 回调/依赖 调用。


// 跟踪变化: 将对象每个属性变化进行监听, val作为闭包（防止死循环）； 但是没有依赖（即改变后的回调）
function defineReactive(obj={}, key='',val){
    Object.defineProperty(
        obj,
        key,
        {
            enumerable: true,
            configurable: true,
            // value: val, // value, 如果设置了 set 或 get, 就不能设置 writable 和 value 中的任何一个,否则就会报错
            get: function(){
                console.log(`获取值`,val);
                // return obj[key];  // 这里不能这样写，因为obj[key] 本身会触发get回调
                return val;   //  这里利利用闭包防止死循环            
            },
            set: function(newVal){
                console.log(`设置值： 旧值 ${val}, 新值为 ${newVal}`);
                // obj[key] = newVal; // 这里不能这样写，因为obj[key] 本身会触发get回调, 同时会再次触发set
                val = newVal;
            }
        }
    )
}


const obj = { name:'你对我你看', num: 15 };

defineReactive(obj,'name', obj.name);

console.log( obj.num );
console.log( obj.name, '111' ); 

obj.name = '难道就我看见你';
obj.name = '难道就我看见你带我去带我去';

console.log(obj.name,666);

/**
 Object.defineProperty() 方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回此对象。
语法：

Object.defineProperty(obj, prop, descriptor)
obj:要定义属性的对象
prop:要定义或修改的属性的名称或 Symbol
descriptor:要定义或修改的属性描述符

当Object.defineProperty设置的prop和obj原有的prop相同时会造成死循环，如下面的initValue属性

            let obj = {
                initValue: '1',
            }
            Object.defineProperty(obj, 'initValue', {
                get(){
                    return obj.initValue
                },
                set(value) {
                    obj.initValue = value
                }
            })
            console.log(obj.initValue)
            obj.initValue = 'zqf'
            console.log(obj.initValue)


console.log(obj.initValue)触发get方法，return obj.initValue再次触发get方法
obj.initValue = 'zqf'触发set方法，obj.initValue = value再次触发set方法

 * 
 * 
 */

