/**
 *  MVP 模式： P(管理器)控制V(多个V的切换)， V不能直接从M中获取数据，需要由P代理
 */

const Web = {
    Model: function(type){
        const data = {
            'a': { name:'带我去带我去', age:15 },
            'b': { name:'dqwmlwdmq', age:16 },
            'default': { name:'', age: '' }
        }

        return data[type]||data.default
    },

    V: function(data={}){
        
        let dom = `<div> <h1>{{name}}</h1> <h2>{{age}}</h2> </div>`
        
        return dom.replace(/\{\{(\w+)\}\}/g, function(subStr,group1){
            return data[group1]||''
        })
    },

    C: function(param){
        const datKeyMap = {
            '1':'a',
            '2':'b',
            'default':'default',
        }

        let data = {}
        if([1,2].includes(param)){
            data = this.Model( datKeyMap[param] )
        }else{
            data = this.Model( datKeyMap.default )
        }

        return this.V(data)
    },
}


console.log( Web.C(1) );
console.log( Web.C(2) );
console.log( Web.C(3) );