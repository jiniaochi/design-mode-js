/**
 * 同步导入模式
 */

(function(globalObj){
    const ModulesMap = {}

    function define(moduleName='',deps=[], callBack){
        if(!moduleName){
            return
        }
        
        if(ModulesMap[moduleName]){
            console.error(`依赖: ${depName} 已存在，请勿重复定义`);
        }else{
            const depObj = {
                modulesName: moduleName,
                deps: deps,
                callBack: callBack,
            }
            ModulesMap[moduleName] = depObj
        }
    }

    function require(deps=[], callBack){
        const resultMap = {}
        for(let depName of deps){
            const depObj = ModulesMap[depName]
            if(!ModulesMap[depName]){
                console.error(`依赖: ${depName} 未找到，请确认已定义`);
                return 
            }
            
            if(!resultMap[depName]){
                // 防止相同依赖重复执行
                const subResults =  require(depObj.deps, depObj.callBack)
                console.log(subResults);
                resultMap[depName] =  ModulesMap[depName].callBack.call(globalObj, subResults)
            }

        }

        return callBack&&callBack.call(globalObj, ...Object.values(resultMap))
    }

    globalObj.define = define
    globalObj.require = require
})( global )


define('test',[], function(){
    console.log('test invoke');

    return {
        test:()=>{ console.log('test'+new Date().toLocaleDateString()); }
    }
})

define('testSub',['test'],function(test){
    console.log(test,'testSub invoke');
    return {
        testSub: ()=>{ console.log('test Sub OKKK'); }
    }
})

// 还有些没做到，例如 重复依赖防止多次引入
require(['test','testSub'],function(test,testSub){
    console.log( test, testSub, '========' );
})