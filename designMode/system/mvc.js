/**
 *  MVC 模式： C控制V(多个V的切换)， V直接从M中获取数据
 */

const Web = {
    Model: function(type){
        const data = {
            'a': { name:'带我去带我去', age:15 },
            'b': { name:'dqwmlwdmq', age:16 },
            'default': { name:'', age: '' }
        }

        return data[type]||data.default
    },

    V: function(page){
        const pageMap = {
            'page1':'a',
            'page2':'b',
            'default':'default',
        }
        let dom = `<div> <h1>{{name}}</h1> <h2>{{age}}</h2> </div>`, data = this.Model( pageMap[page]||'default' )
        
        return dom.replace(/\{\{(\w+)\}\}/g, function(subStr,group1){
            return data[group1]||''
        })
    },

    C: function(param){
        if(param==1){
            return this.V('page1')
        }else if(param==2){
            return this.V('page2')
        }else{
            return this.V('default')
        }
    },
}


console.log( Web.C(1) );
console.log( Web.C(2) );
console.log( Web.C(3) );