/**
  数据访问模式： data aceess object(DAO)
 统计调度数据库Index DB、
 本地缓存LocalStorage/SessionStorage、cookie
 */
// 以localstorage为例

class localStorageDAO {
    seprator = '@@__@@';// 防止魔术字符串出现

    // 加密
    encode(str){
        return btoa(str);
    }
    // 解密
    decode(base64Str){
        return atob(base64Str);
    }

    // 增 和 改
    set(obj,key,expireDate){
        if(obj){
            if(!expireDate){
                expireDate = new Date();
                // 默认3天后过期
                expireDate.setDate(expireDate.getDate()+3);
            }
            localStorage.setItem( key, this.encode(JSON.stringify(obj)+this.seprator+expireDate) );
        }
    }

    get(key){
        if(localStorage.getItem(key)){
            let content = this.decode(localStorage.getItem(key)).split(this.seprator), expireDate = new Date(content[1]);
            if(expireDate>=new Date()){
                return content[0];
            } 
        }

        return null;
    }

    remove(key){
        if(localStorage.getItem(key)){
            localStorage.removeItem(key);
        }
    }
}



