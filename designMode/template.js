/** 模板模式： 字符串替换key */

// 模板格式化 #{test}# 样式, 正则表达式里面没事不要加空格
function transformTemplate(tplStr,data){
    return tplStr.replace(/\{#(\w+)#\}/g, function(match,key){
        // return match+"||"
        return data[key]?data[key]:''
    })
}


const requestData = {
    div:'这是一个div',
    span:'这是一个span',
    test:'test内容',
}

const TemaplateStrs = {
    div: `<div>{#test#}</div>`
}

console.log( transformTemplate(TemaplateStrs.div, requestData) );


