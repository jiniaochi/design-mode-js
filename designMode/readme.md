# 关于

这里介绍的是JS的技巧性、架构性的设计模式，和js业务相关；不包括GOF23的设计模式。

## 技巧型

### 1.链模式

JQuery 式的链式调用， 实现一套自己的JQuery。

#### 函数柯里化和链模式

函数柯里化是参数不是一次性传入，而是多次传入，不要和链式混了。

```js
// 链式
$(this).on(...).on(...).on(...);

// 普通的add函数
function add(x, y) {
    return x + y
}
 
// Currying后
function curryingAdd(x) {
    return function (y) {
        return x + y
    }
}
 
add(1, 2)           // 3
curryingAdd(1)(2)   // 3
```

实际上就是把add函数的x，y两个参数变成了先用一个函数接收x然后返回一个函数去处理y参数。现在思路应该就比较清晰了，就是**只传递给函数一部分参数来调用它，让它返回一个函数去处理剩下的参数**。

柯里化的特点：

- 参数复用（固定易变因素）
- 延迟执行
- 提前返回

### 2.委托模式

如HTML事件的**事件委托**,父组件集中处理冒泡上来的事件，减少事件绑定数目，事件多了，内存消耗就多了。
同时，时间委托也可以预言未来，即不存在的元素，未来某一段时间插入了DOM书中，也可以绑定到。

或者一个页面的多次请求，可以单独提出成为一个委托对象，这样就能一次获取，然后存在某地（闭包或缓存），方便其他组件的使用。

### 3.数据访问模式(DAO，data access object)

抽象和封装对数据源的访问和存储 成为一个DAO的类（封装数据库的增删改查），为我们提供简单而统一的接口； 本地缓存`localStorage`可以设置一个过期时间(仿效Cookie), 有点私密性的可以用Base64对称加密或其他对称、非对称加密。

### 4.节流模式

防抖函数（要清除以前的计数器）、节流函数、图片懒加载。

### 5.模板模式

根据后端数据的选择渲染前端HTML结构，前端可以使用模板技术，一次性插入DOM，很适用于JQuery体系

```js
// 模板替换, replace 第二个参数为函数(妙啊)
function format(str,data){
    return str.replace(/\{#(\w+)#\}/g,function(match,key){
        return typeof data[key]==='undefined'?'':data[key];
    })
}

format( `<p>{#name#}</p><span>{#test#}</span>` , { name: 'Julius' }) // '<p>Julius</p><span>111</span>

// replace 第二个参数为函数时：当匹配执行后，该函数就会执行，函数的返回值作为 被替换的字符串； 第一个参数是匹配的子串，随后就是匹配的多个文组
function replacer(match, p1, p2, p3, offset, string) {
  // p1为非数字，p2为数组，p3为非字母
  return [p1, p2, p3].join(' - ');
}
var newString = 'abc12345#$*%'.replace(/([^\d]*)(\d*)([^\w]*)/, replacer);
console.log(newString);  // abc - 12345 - #$*%

```

### 6.惰性模式

一般函数加载就执行，如

```js
A.on = function(dom,type,callBack){
    if(window.addEventListener){
        dom.addEventListener(type, callBack)
        
    }else{
        dom['on'+type] = callBack
    }
}
```

每一次`on`操作都要检测一下浏览器的`addEventlistener`能力，而惰性执行则不一样：

```js
//  惰性函数， 方式1：立即执行函数
A.on = (function(dom,type,callBack){
    if(window.addEventListener){
        // 重新定义
        return function (dom,type,callBack){
            dom.addEventListener(type, callBack)
        }
    }else{
        return function (dom,type,callBack){
            dom['on'+type] = callBack
        }
    }

})

//  方式2 惰性执行
A.on = function(dom,type,callBack){
    if(window.addEventListener){
        // 重新定义
        A.on = function (dom,type,callBack){
            dom.addEventListener(type, callBack)
        }
    }else{
        A.on = function (dom,type,callBack){
            dom['on'+type] = callBack
        }
    }

    // 妙啊，执行定义后的on
    A.on(dom,type, callback)
}
```

第一次`on`操作都要检测一下浏览器的`addEventlistener`能力，后面因为`A.on`被改写，所以不需要再检测

惰性模式在`Jquery`中的 **事件绑定**和**XMLHttpRequest**中检测浏览器差异又使用，主要是为了解决差异化的API(如AJAX在标准浏览器中使用XMLHttpRequest，而在IE中使用ActiveXObject )

### 7.参与者模式

函数绑定和函数柯里化：

### 8.等待者模式

JQuery 的异步`Promise`实现，异步任务之间的同步； 手写`Promise`（`queueMicrotask`微任务队列或`setTimeOut`）

---

## 架构型

### js模块化（Model模块化）

类似require.js的require和define方法，将js模块化开发

#### 同步模块模式：SMD

前端模块化开发，模块管理器：同步加载，同步加载默认所有模块都是同步的，模块一加载方法执行后不论结果，直接进行下一步（适合服务端）

#### 异步模块模式：AMD

前端模块化开发，模块管理器：异步加载，异步加载需要确认当前需要的模块已经全部加载完成，然后再执行下一步（适合浏览器）

### 视图（View）模块化

#### Widget 模式

Web Widget是一块可以在任何页面执行的代码块，Widget模式是指借用Web Widget思想，将页面拆分为 组件/部件, 针对组件开发，最终合成完整的页面。

### MVC 软件架构

#### MVC模式

软件结构分层，M 模型、V 视图、C控制器， V视图渲染页面，M 模型存放和控制数据，C 控制页视图的渲染（其中V视图层可以直接从M模型层获取数据）。

#### MVP模式

软件结构分层，M 模型、V 视图、P管理器，类似MVC，只是V视图层不可以直接从M模型层获取数据，而是要经过P管理器层，即所有的数据交互都要通过P管理器层完成。

#### MVVM模式

软件结构分层，M 模型、V 视图、VM视图模型，类似MVP模式，区别在于VM视图模型层监听了M模型层和V视图层，V视图层的变化会经过VM通知到M模型层（事件监听），
M模型层的变化也会通知到视图层（重新渲染改变部分）；而且也解决了MVP模式P管理器臃肿的问题，这里的VM层功能更偏少（仅起到一个中间通知的作用）。
