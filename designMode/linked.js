/**
   链模式： 实现Jquey类似的链式调用， 实现方法有init（对象获取）、on、extend
 */
var J = function(selector){
    // 为了让获取的元素互补性影响，采取new构造新对象
    return new J.prototype.init(selector);
};

J.prototype.init = function(selector){
    // 也可以使用一个新对象, 也可以使用this
    let domArr = document.querySelectorAll(selector);
    for(let i=0;i<domArr.length;i++){
        this[i] = domArr[i];
    }

    this.length = domArr.length;
}

J.prototype.on = function(eventName,callback){
    for(let i=0;i<this.length;i++){
        this[i].addEventListener(eventName,callback);
    }

    return this;
}

J.prototype.addStyle = function(style){
    if(style){
        let styleCntent = style.replace(/;/g,'').split(':');
        alert(styleCntent[0])
        for(let i=0;i<this.length;i++){
            this[i].style[styleCntent[0]] = styleCntent[1];
        }
    }

    return this;
}

J.prototype.extends = function(sourceObj){
    if(sourceObj&&Object.toString.call(sourceObj)=='[object Object]'){
        // 先浅度克隆
        Object.assign(J.prototype, sourceObj);
    }
}


// 让init产生的对象 自带J.prototype, 就是继承J.prototype
// 原型链 式 继承
J.prototype.init.prototype = J.prototype;
