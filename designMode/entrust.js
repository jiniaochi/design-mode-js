/**
    委托模式
    1.事件代理
    2.页面操作聚合
 */

// 1.DOM事件委托
function domEntrust() {
    // Make a list
    var ul = document.createElement('ul');
    document.body.appendChild(ul);

    var li1 = document.createElement('li');
    var li2 = document.createElement('li');
    ul.appendChild(li1);
    ul.appendChild(li2);

    function hide(e) {
        // e.target 引用着 <li> 元素，target指向事件源
        // 不像 e.currentTarget 引用/指向 着其父级的 <ul> 元素。也就是currentTarget指向绑定事件的当前元素
        e.target.style.visibility = 'hidden';
        e.currentTarget.style.color = '#F5F5F5';
    }

    // 添加监听事件到列表，当每个 <li> 被点击的时候都会触发。不需要每一个li元素均绑定事件
    ul.addEventListener('click', hide, false);

}


function mergeRequest(){
    //一个系统存在多相同个请求合并一起处理

    // top.js处理头部数据
    $.post('/data').then((res)=>handleTop())

    //bottom.js处理尾部数据
    $.post('/data').then((res)=>hanldeBottom())

    //合并
    const handleObj = {
        top: handleTop,
        bottom: hanldeBottom,
    }

    $.post('./data').then((res)=>{
        for(let key of Object.keys(handleObj)){
            handleObj[key].call(this,res)
        }
    })
}




