/**
 * 惰性模式： 文件加载时就确定执行环境的分支 重新定义函数， 如不同浏览器的 ajax或addEventListener
 */

const J = {}

// 方式一: 立即执行函数
J.on = (function(){
    if(document.addEventListener){
        J.on = function(dom,type,callBack){
            //  一般浏览器
            dom.addEventListener(type,callBack)
        }
    }else if(document.attachEvent){
        // IE浏览器
        J.on = function(dom,type,callBack){
            dom.attachEvent('on'+type,callBack)
        }
    }else{
        // 其他情况
        J.on = function(dom,type,callBack){
            dom['on'+type] = callBack
        }
    }
})()


// 方式二 惰性执行
J.createXHR = function(){
    if(typeof XMLHttpRequest !='undefined'){
        // 一般浏览器ajax对象
        J.createXHR = function(){
            return new XMLHttpRequest()
        }
    }else if(typeof ActiveXObject){
        // IE浏览器的ajax对象
        J.createXHR = function(){
            // ...
        }
    }else{
        J.createXHR = function(){
            throw new Error('不存在XHR对象')
        }
    }

    // 神之一笔，再次调用改造后的createXHR方法
    return J.createXHR()
}


