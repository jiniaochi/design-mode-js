/**
 * 寄生继承：本质和原型式继承一样
 * 在原型式基础上，增强属性
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
    console.log( this);
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

// Objext.create()的失效
function generateObj(parentObj){
    function F(){} // F相当于 原型链式继承的ChildClass
    F.prototype = parentObj;
    F.prototype.construcor = F;
    return new F();
}

function ChildClass(name,adddress){
    // 报错： Invalid left-hand side in assignment
    // this = Object.create(new ParentClass(name,15));

    /**Objext.create的形式 */
    // let obj = Object.create(new ParentClass(name,15));

    /**自己实现Object.create的形式 */
    let obj = generateObj(new ParentClass(name,15));

    // 增强
    obj.adddress = adddress;
    return obj;
}
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}

let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log();
