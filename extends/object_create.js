/**
 * 借助原型 `Object.create`，可以基于已有的对象创建新的对象，同时还不必因此创建 子类构造函数
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
    console.log( this);
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

// Objext.create()的失效
function generateObj(parentObj){
    function F(){} // F相当于 原型链式继承的ChildClass, 不过迁移到了函数内部
    F.prototype = parentObj;
    F.prototype.construcor = F;
    return new F();
}

function ChildClass(name,adddress){
    // 报错： Invalid left-hand side in assignment
    // this = Object.create(new ParentClass(name,15));

    /**Objext.create的形式 */
    // let obj = Object.create(new ParentClass(name,15));

    /**自己实现Object.create的形式 */
    let obj = generateObj(new ParentClass(name,15));
    return obj;
}

let child = new ChildClass('jinxiaochi',18);
child.log();


