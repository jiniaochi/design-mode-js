/**
 * es6继承
 * 
**/

class ParentClass{
    constructor(name,age=15){
        this.name = name;
        this.age = age;
    }

    log(){
        console.log( `name=${this.name}; age=${this.age}` );
    }
}

class ChildClass extends ParentClass{
    constructor(name,address){
        super(name);
        this.name = name;
        this.adddress = address;
    }
    log(){
        super.log() // name=sadnqwkjd; age=15
        console.log( super.name, super.age ) // undefined undefined
        console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
    }
}

let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();
console.log( child, Object.getPrototypeOf(child) );