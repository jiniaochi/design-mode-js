# JavaScript常见的几种继承实现方式

## 1. 原型链的继承

子类的原型等于父类实例。

```js
function ParentClass(name,age){
    this.name = name;
    this.age = age;
}

ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    this.name = name;
    this.adddress = adddress;
}



ChildClass.prototype = new ParentClass();
// 子类原型特有属性需要后面再设置，尤其是contructor构造函数需要更正
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}` );
}
ChildClass.prototype.constructor = ChildClass; // 不改的话构造函数就是 ParentClass


let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log();

/**
 * 额外： instanceof用于检测构造函数的prototype  属性是否出现在实例对象的原型链上 （两边都是prototype，两边都是对象）。
 */
ParentClass.prototype =  Number;
console.log( new ParentClass('sad',12) instanceof Number ); // false

ParentClass.prototype =  Number.prototype;
console.log( new ParentClass('sad',12) instanceof Number ); // true
```

## 2. 构造函数继承

子类构造函数调用父类构造函数（使用`call`或`apply`方法）。

```js
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    ParentClass.call(this,name);
    this.adddress = adddress;
}

ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}

let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log();
```

## 3. 组合使用 原型继承和构造函数继承（组合使用1和2，即 方法3=方法2+方法1 ）

使用原型链实现对原型属性和方法的继承，通过借用构造函数来实现对实例属性的继承。

```js
/**
 * 原型链 组合模式
 * 父类原型-> 原型链模式
 * 父类自有属性-> 构造函数模式
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}


function ChildClass(name,adddress){
    ParentClass.call(this,name);
    this.adddress = adddress;
}

ChildClass.prototype = new ParentClass()
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}
ChildClass.prototype.constructor = ChildClass;

let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log(); // name=jinxiaochi; adddress=GuangDong Shenzhen; age=10
```

## 4. 原型式继承

借助原型，可以基于已有的对象创建新的对象，而不必创建构造函数； 使用`Object.create()`。

```js
/**
 * 借助原型 `Object.create`，可以基于已有的对象创建新的对象，同时还不必因此创建 子类构造函数
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
    console.log( this);
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

// Objext.create()的失效
function generateObj(parentObj){
    function F(){
    } // F相当于 原型链式继承的ChildClass
    F.prototype = parentObj;
    F.prototype.construcor = F;
    return new F();
}

function ChildClass(name,adddress){
    // 报错： Invalid left-hand side in assignment
    // this = Object.create(new ParentClass(name,15));

    /**Objext.create的形式 */
    // let obj = Object.create(new ParentClass(name,15));

    /**自己实现Object.create的形式 */
    let obj = generateObj(new ParentClass(name,15));
    return obj;
}

let child = new ChildClass('jinxiaochi',18);
child.log();
```

## 5. 寄生式继承

创建一个仅用于封装继承过程的函数，该函数内部以某种方式来增强完成继承（之前一般用原型式继承）后对象。本质和原型式继承一样

```js
/**
 * 寄生继承：本质和原型式继承一样
 * 在原型式基础上，增强属性
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
    console.log( this);
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

// Objext.create()的失效
function generateObj(parentObj){
    function F(){} // F相当于 原型链式继承的ChildClass
    F.prototype = parentObj;
    F.prototype.construcor = F;
    return new F();
}

function ChildClass(name,adddress){
    // 报错： Invalid left-hand side in assignment
    // this = Object.create(new ParentClass(name,15));

    /**Objext.create的形式 */
    // let obj = Object.create(new ParentClass(name,15));

    /**自己实现Object.create的形式 */
    let obj = generateObj(new ParentClass(name,15));

    // 增强
    obj.adddress = adddress;
    return obj;
}
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}

let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log();
```

## 6. 寄生组合式继承

将组合式和寄生式结合起来； 父类自身属性用构造函数继承，父类原型属性用寄生式继承。

```js
/**
 * 将组合式和寄生式结合起来； 父类自身属性用构造函数继承，父类原型属性用寄生式继承。
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    let obj = Object.create( ParentClass.prototype )
    obj.adddress = adddress;
    // 自身的原型属性就被抹掉了
    obj.log = function(){
        console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
    }

    ParentClass.call(obj,name);
    return obj;
}

// 此方法不生效
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}


let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();
Object.getPrototypeOf(child).log.call(child);
```

---

## 7. 混入方式继承多个对象： 额外的变式，算是寄生组合的拓展变化

在寄生组合方法后，使用`Object.assin()`混合 另外一个类的原型 进来。

```js
/**
 * 在寄生组合方法后，使用`Object.assin()`混合 另外一个类的原型 进来。
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    let obj = Object.create( Object.assign( {},ParentClass.prototype, ChildClass.prototype, Another.prototype ) )
    obj.adddress = adddress;

    ParentClass.call(obj,name);
    return obj;
}

// 因为 Object.assign() 聚合了ChildClass.prototype， 故存在
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}

function Another(){}
Another.prototype.test = function(){ 
    console.log('test');
}

let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();
Object.getPrototypeOf(child).log.call(child);
```

## 8. ES6 继承extends ： ES拓展, 单继承不能多继承

ES5 的继承机制，是先创造一个独立的子类的实例对象，然后再将父类的方法添加到这个对象上面，即“实例在前，继承在后”。

ES6 的继承机制，**则是先将父类的属性和方法，加到一个空的对象上面，然后再将该对象作为子类的实例**，即“继承在前，实例在后”。这就是为什么 ES6 的继承必须先调用super()方法，因为这一步会生成一个继承父类的this对象，没有这一步就无法继承父类。

```js
/**
 * es6继承
 * 
**/

class ParentClass{
    constructor(name,age=null){
        this.name = name;
        this.age = null;
    }

    log(){
        console.log( `name=${this.name}; age=${this.age}` );
    }
}

class ChildClass extends ParentClass{
    constructor(name,address){
        super(name);
        this.name = name;
        this.adddress = address;
    }
    log(){
        super.log() // name=sadnqwkjd; age=15
        console.log( super.name, super.age ) // undefined undefined
        console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
    }
}

let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();

// 子组件原型指向 空的父节点
console.log( child, Object.getPrototypeOf(child) );// ChildClass { name: 'sadnqwkjd', age: null, adddress: 'BHASN' } ParentClass {}
```
