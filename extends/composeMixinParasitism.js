/**
 * 在寄生组合方法后，使用`Object.assin()`混合 另外一个类的原型 进来。
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    let obj = Object.create( Object.assign( {},ParentClass.prototype, ChildClass.prototype, Another.prototype ) )
    obj.adddress = adddress;

    ParentClass.call(obj,name);
    return obj;
}

// 因为 Object.assign() 聚合了ChildClass.prototype， 故存在
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}

function Another(){}
Another.prototype.test = function(){ 
    console.log('test');
}

let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();
Object.getPrototypeOf(child).log.call(child);