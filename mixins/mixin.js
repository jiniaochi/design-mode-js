 /**
 * 混入 也成为多重继承
 * 暂未区分 原型属性和非原型属性
 */

 function maxin( targetObj, source={}, ...keys ){
    if(keys.length){
        for(let i=0;i<keys.length;i++){
            let type = typeof source[keys[i]];
            
            if(type==null||['function','number','boolean','symbol'].includes(type)){
                targetObj[keys[i]] = source[keys[i]]
            }else if(source[keys[i]] instanceof Date){
                targetObj[keys[i]] = new Date(source[keys[i]])
            }else{
                // 深度克隆
                targetObj[keys[i]] = JSON.stringify( JSON.parse() )
            }
        }
    }else{
        for(let key in source){
            let type = typeof source[key];
            if(type==null||['function','number','boolean','symbol'].includes(type)){
                targetObj[keys] = source[key]
            }else if(source[key] instanceof Date){
                targetObj[keys] = new Date(source[key])
            }else{
                // 深度克隆
                targetObj[keys] = JSON.stringify( JSON.parse() )
            }
        }
    }
 }